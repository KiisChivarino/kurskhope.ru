        <div class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 book"><a href="{{ asset('files/kursk_hope_book.pdf') }}" target="_blank"><strong>Энциклопедия (скачать)</strong></a></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 copyright"><span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span> Все права защищены</div>
                            <div class="col-md-3 col-sm-3 col-xs-12 contacts">
                                <a href="https://vk.com/talanty_kursk" target="_blank"><span class="contacts-icon vk"></span></a>
                                <a href="https://www.youtube.com/channel/UCRTfx_ZOXDRlbKTgi9uKsiA" target="_blank"><span class="contacts-icon youtube" target="_blank"></span></a>
                                <a href="mailto:kurskhope@yandex.ru" target="_blank"><span class="contacts-icon email"></span></a>
                            </div>
                        </div>
                    </div>
        </div>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter45073223 = new Ya.Metrika({
                            id:45073223,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/45073223" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->