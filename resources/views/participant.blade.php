@extends('layouts.layout')

@section('header')
	@parent
    <link rel="stylesheet" type="text/css" href="{{ asset('css/participant.css') }}" />
@endsection

@section('content')
    <div class="content participant">
        <div class="container breadcrumbs">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/">Главная</a></li>
                        <li><a href="/participants/">Участники</a></li>
                        <li><a href="/participants/{{$currentCategory}}">{{$currentCategoryTitle}}</a></li>
                        <li>{{$actor->actor_name}}</li>
                    </ol>
                </div>
            </div>
        </div>
    <div class="container content-top">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 content-title">
                <h1>{{$actor->actor_name}} @if(!empty($actor->birth_date))({{date('d.m.Y', strtotime($actor->birth_date))}})@endif</h1>
            </div> 
        </div>
    </div>
    <!--<div class="container participant-info">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 main-photo">
                <img src="/images/actors/fullSize/{{$actor->image}}" alt="{{$actor->actor_name}}"/>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12 profile">
                <p>
                    <span class="pinfo-label">Направление: </span><span>{{$actor->category_name}}</span><br/>
                    <span class="pinfo-label">Город: </span><span>Курск</span><br/>
                    <span class="pinfo-label">Школы: </span><span></span><br/>
                    <span class="pinfo-label">Вузы: </span><span></span><br/>
                    <span class="pinfo-label">Родители: </span><span></span><br/>
                    <span class="pinfo-label">Учителя: </span><span></span><br/>
                    <span class="pinfo-label">Достижения: </span><span></span><br/>
                    <br/>
                    <span class="pinfo-label">Email: </span><span></span><br/>
                    <span class="pinfo-label">Scype: </span><span></span><br/>
                    <span class="pinfo-label">Соц. сети: </span><span></span>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h2>Портфолио</h2>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                {!!$actor->article!!}
            </div>
        </div>
    </div>-->
    <div class="container participant-info">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 main-photo">
                <img src="/images/actors/fullSize/{{$actor->image}}" alt="{{$actor->actor_name}}"/>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 profile">
                        <p>
                            <span class="pinfo-label">Направление: </span><span>{{$actor->category_name}}</span><br/>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 article">
                        {!!$actor->article!!}
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('footer')
	@parent
@endsection