<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    
    public function search($search=null){
        
        $search = urldecode($search);
        $searchFilter = " WHERE a.name LIKE '%$search%' ";
        
        /*↓моя пагинация↓*/
            $countPerPage = 16;
             $page = (int) \Request::get('page');
            if($page>0) $currentPage = $page; else $currentPage = 1;
            $paginationFilter = "";
            $paginationFilter = " LIMIT 0,$countPerPage ";
            if ($page>0) {
                $leftLimit = ($page-1)*$countPerPage;
                $paginationFilter = " LIMIT $leftLimit,$countPerPage ";
            }
            
            
            
            /*↑моя пагинация↑*/

            /*вытаскиваю категории*/
            $categories = DB::select("SELECT id, name, title from categories");
            /*вытаскиваю категории*/
            
            /*определяю максимальное количество страниц*/
            $actorsCount = DB::select("
                                    SELECT count(*) as cnt
                                           FROM participants AS p
                                    LEFT JOIN actors AS a ON p.id=a.id
                                    LEFT JOIN categories AS c ON p.categories_id=c.id
                                    LEFT JOIN levels AS l ON p.levels_id=l.id
                                    $searchFilter
                                ");

            $maxPage = round($actorsCount[0]->cnt/$countPerPage, 0, PHP_ROUND_HALF_UP);
            /*определяю максимальное количество страниц*/

            $actors = DB::select("
            						SELECT p.id,
                                           a.name as actor_name, 
            							   a.image, 
            							   p.birth_date,
                                           c.name as category_name,
            							   c.title as category_title, 
            							   l.title as level_name 
            							   FROM participants AS p
            						LEFT JOIN actors AS a ON p.id=a.id
            						LEFT JOIN categories AS c ON p.categories_id=c.id
                                    LEFT JOIN levels AS l ON p.levels_id=l.id
                                    $searchFilter
                                    ORDER BY a.name
                                    $paginationFilter;
            					");
      
            $array = array(
            	'actors'=>$actors,
                'categories'=>$categories,
                'currentPage'=>$currentPage,
                'maxPage'=>$maxPage
            );
        
        return view('search',$array);
        
    }
}
