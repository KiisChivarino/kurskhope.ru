<?php 
    namespace App\Http\Controllers;
    use Illuminate\Support\Facades\DB;
    use App\Http\Controllers\Controller;
    class ParticipantsController extends Controller {
        //public $_actors;
        public function show($category='all', $level='all'){
            
            /*↓моя пагинация↓*/
            $countPerPage = 16;
             $page = (int) \Request::get('page');
            if($page>0) $currentPage = $page; else $currentPage = 1;
            $paginationFilter = "";
            $paginationFilter = " LIMIT 0,$countPerPage ";
            if ($page>0) {
                $leftLimit = ($page-1)*$countPerPage;
                $paginationFilter = " LIMIT $leftLimit,$countPerPage ";
            }
            
            
            
            /*↑моя пагинация↑*/

            /*вытаскиваю категории*/
            $categories = DB::select("SELECT id, name, title from categories");
            $categoryFilter = '';
            $levelFilter = '';
            /*вытаскиваю категории*/


            if($category!='all') {
                $categoryFilter = ' WHERE c.name='.'"'.$category.'"';
                if($level!='all') $levelFilter = ' AND l.name='.'"'.$level.'"';
            }
            else{
                if($level!='all') $levelFilter = ' WHERE l.name='.'"'.$level.'"';
            }
            
            /*определяю максимальное количество страниц*/
            $actorsCount = DB::select("
                                    SELECT count(*) as cnt
                                           FROM participants AS p
                                    LEFT JOIN actors AS a ON p.id=a.id
                                    LEFT JOIN categories AS c ON p.categories_id=c.id
                                    LEFT JOIN levels AS l ON p.levels_id=l.id
                                    $categoryFilter
                                    $levelFilter
                                ");

            $maxPage = round($actorsCount[0]->cnt/$countPerPage, 0, PHP_ROUND_HALF_UP)+1;
            /*определяю максимальное количество страниц*/

            $actors = DB::select("
            						SELECT p.id,
                                           a.name as actor_name, 
            							   a.image, 
            							   p.birth_date,
                                           c.name as category_name,
            							   c.title as category_title, 
            							   l.title as level_name 
            							   FROM participants AS p
            						LEFT JOIN actors AS a ON p.id=a.id
            						LEFT JOIN categories AS c ON p.categories_id=c.id
                                    LEFT JOIN levels AS l ON p.levels_id=l.id
                                    $categoryFilter
                                    $levelFilter
                                    ORDER BY a.name
                                    $paginationFilter;
            					");
            
            
            $currentCategoryTitle = 'Все участники';
            foreach($categories as $category_item){ 
                if($category_item->name == $category){
                    $currentCategoryTitle = $category_item->title;
                    break;
                }
            }
            $array = array(
            	'actors'=>$actors,
                'categories'=>$categories,
                'currentCategory'=>$category,
                'currentCategoryTitle'=>$currentCategoryTitle,
                'currentLevel'=>$level,
                'currentPage'=>$currentPage,
                'maxPage'=>$maxPage
            );
            return view('participants',$array);
            
        }
    }
    
?>