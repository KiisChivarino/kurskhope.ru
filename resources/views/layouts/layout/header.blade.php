        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3 col-xs-12 logo-img"><img src="{{ asset('images/logo.png') }}"/></div>
                    <div class="col-md-8 col-sm-9 col-xs-12 title"><h1>Будущее <br/>соловьиного <br/> края</h1></div>
                    <div class="col-md-2 hidden-sm hidden-xs birds"><img src="{{ asset('images/birds.png') }}"/></div>
                </div>
            </div>
            @section('navMenu') 
                @include ('layouts.layout.navbar')
            @show
        </div>