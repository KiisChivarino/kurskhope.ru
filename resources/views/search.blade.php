@extends('layouts.layout')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/participants.css') }}" />
@endsection
@section('header')
	@parent
@endsection
@section('content')
        <div class="content participants">
            
            <div class="container participants-list">
                @php ($i = 0)
                @foreach($actors as $actor)
                @if($i==0)<div class="row">@endif
            		<a href="/participants/{{$actor->category_name}}/{{$actor->id}}">
                    <div class="col-md-3 col-sm-6 col-xs-12 patricipant">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-6 image">
                                    <img src="/images/actors/fullSize/{{$actor->image}}" alt="{{$actor->actor_name}}"/>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6 info">
                                <p><span class="name">{{$actor->actor_name}}</span><br/>
                                    <span class="dt-birth">{{date('d.m.Y', strtotime($actor->birth_date))}}</span><br/>
                                    <span class="category">{{$actor->category_title}}</span><br/>
                                    <!--<span class="level">{{$actor->level_name}}</span>-->
                                </p>
                            </div>
                        </div>
                    </div>
                    </a>
                @php ($i++)
                @if($i>3) </div> @php ($i = 0) @endif
                @endforeach
                @if($i<3 and $i>0) </div> @endif
            </div>
            <div class="container page-navigation">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <nav aria-label="Page navigation">
                          <ul class="pagination">
                            <li>
                              @if($currentPage>1)<a href="/participants/{{$currentCategory}}/{{$currentLevel}}?page={{$currentPage-1}}" aria-label="Previous">@endif
                                <span @if($currentPage<=1) class="span-black" @endif aria-hidden="true">&laquo;</span>
                              @if($currentPage>0)</a>@endif
                            </li>
                            <!--<li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>-->
                            <li>
                              @if($currentPage<$maxPage)<a href="/participants/{{$currentCategory}}/{{$currentLevel}}?page={{$currentPage+1}}" aria-label="Next">@endif
                                <span @if($currentPage>=$maxPage) class="span-black" @endif aria-hidden="true">&raquo;</span>
                              @if($currentPage<$maxPage)</a>@endif
                            </li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>
@endsection
@section('footer')
	@parent
@endsection