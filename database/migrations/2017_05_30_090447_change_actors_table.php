<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeActorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('actors')){
			Schema::table('actors', function (Blueprint $table) {
            	//
            	$table->unsignedTinyInteger('types_id');
            	$table->unsignedTinyInteger('roles_id');
        	});			
		}
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actors', function (Blueprint $table) {
            //
            $table->dropColumn('types_id');
            $table->dropColumn('roles_id');
        });
    }
}
