<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="2a092f4a106b1d50" />
        <meta name="google-site-verification" content="dFySSA5GbdTgVeCRqkhcqBaScL7xasW5nyznMztmN0U" />
        <title>Будущее соловьиного края - энциклопедия</title>
        
        <meta name="description" content="Официальный сайт форума Виктора Карамышева «Курск - 21 век». Здесь можно найти электронную книгу, фотографии, статьи об одаренных детях-курянах"/>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        @section('styles')
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/all.css') }}" />
        @show
    </head>
    <body>
        @section('header')
            @include ('layouts.layout.header')
        @show
        
        @yield('content')
        
        @section('footer')
            @include ('layouts.layout.footer')
             <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
            <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('js/jquery-migrate-1.4.1.min.js') }}"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="{{ asset('js/bootstrap.js') }}"></script>
            
        @show
    </body>
</html>