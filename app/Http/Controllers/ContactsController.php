<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function show(){
        $categories = DB::select("SELECT id, name, title, image, article from categories");

        $array = array(
                    'categories'=>$categories,
                );
                return view('contacts',$array);
    }
    public function form() {
      $user = array(
        'email' => Input::get('email')
      );

      $data = array(
        'email' => Input::get('email'),
        'message_body' => Input::get('message')
      );

      Mail::queue('emails.contact', $data, function($message) use ($user) {
        $message->to('kurskhope@yandex.ru')
        ->replyTo($user['email'])
        ->subject('Contact form request');
        });

            return Redirect::action('ContactsController@show')->with('success', 'You\'ve successfully sent a message!');
    }
}
