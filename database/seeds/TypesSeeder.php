<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
        									['name'=>'company','title'=>'Организация'],
        									['name'=>'person','title'=>'Физическое лицо'],
        								  ]);
    }
}
