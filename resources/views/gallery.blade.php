@extends('layouts.layout')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/gallery.css') }}" />
    <link rel="stylesheet" href="{{ asset('js/fancybox/jquery.fancybox.min.css') }}" type="text/css" media="screen" />
   
    
@endsection
@section('header')
	@parent
@endsection
@section('content')
        <div class="clearfix mosaicflow"> 
          @foreach($images as $image)
          <div class="mosaicflow__item">
            <a class="fancyimage" data-fancybox-group="group" rel="group" href="{{ asset('images/gallery/fullSize/'.$image) }}"><img src="{{ asset('images/gallery/mini/'.$image) }}" /></a> 
          </div>
          @endforeach
        </div>
@endsection
@section('footer')
	@parent
    <script src="{{ asset('js/fancybox/jquery.fancybox.min.js') }}"></script> 
    <script src="{{ asset('js/jmosaicflow/jquery.mosaicflow.min.js') }}"></script> 
    <script type="text/javascript"> 
      jQuery(document).ready(function() { 
        jQuery("a.fancyimage").fancybox({}); 
      }); 
    </script>
@endsection