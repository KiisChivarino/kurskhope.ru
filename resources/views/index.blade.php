@extends('layouts.layout')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
    
    <link rel="stylesheet" href="{{ asset('js/fancybox/jquery.fancybox.min.css') }}" type="text/css" media="screen" />
    
     <link rel="stylesheet" type="text/css" href="{{ asset('js/slick/slick.css') }}"/>
   <link rel="stylesheet" type="text/css" href="{{ asset('js/slick/slick-theme.css') }}"/>
@endsection
@section('header')
	@parent
@endsection
@section('content')
<div class="content main">
    <div class="container-fluid">
        <div class="row">
            <div class="content-left">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="participants-categories">
                        <div class="row">
                            <ul class="list-group">
                                @foreach($categories as $category)
                                <div class="@if($category->name == 'all')
                                            col-md-12 @else col-md-6 
                                            @endif
                                            col-sm-12 col-xs-12">
                                    <a href="/{{$category->name}}">
                                        <li class="list-group-item">{{$category->title}}</li>
                                    </a> 
                                </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="gallery">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="gallery-slider">
                                    @foreach($main_photo as $image)
                                    <div class="mosaicflow__item">
                                        <a class="fancyimage" data-fancybox-group="group" rel="group" href="{{ asset('images/main_photo/fullSize/'.$image) }}">
                                            <img src="{{ asset('images/main_photo/mini/'.$image) }}" />
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="participants">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="content-title">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="color: #b72504; margin-bottom: 30px;">{{$currentCategory->title}}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="greeting">
                        <div class="greeting-photo">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <img src="/images/categories/{{$currentCategory->image}}"/>
                                </div>
                            </div>
                        </div>
                        <div class="greeting-writer">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!!$currentCategory->greeting_writer!!}
                                </div>
                            </div>
                        </div>
                        <div class="greeting-text">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    {!!$currentCategory->article!!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="participants-video">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="{{$currentCategory->video}}"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="participants-children">
                        @php ($icol = 0)
                        @php ($irow = 0)
                        @foreach($actors as $actor)
                        @if($icol==0)
                        <div class="row 
                                    @if($irow>2)
                                       hidden-xs 
                                    @endif">
                        @endif
                            <div class="col-md-1 col-sm-1 col-xs-2 actor-link">
                                <a href="/participants/{{$actor->category_name}}/{{$actor->id}}" >
                                    <img src="/images/actors/mini/{{$actor->mini_image}}" alt="{{$actor->actor_name}}" /> 
                                </a>
                                <div class="actor-info">
                                    <p>{{$actor->actor_name}}</p><hr/>
                                    @if(!empty($actor->birth_date))
                                    <p>{{date('d.m.Y', strtotime($actor->birth_date))}}</p><hr/>
                                    @endif
                                    <p>{{$actor->category_title}}</p><hr/>
                                    <p>{{$actor->level_name}}</p>
                                </div>
                            </div>
                        @php ($icol++)
                        @if($icol>11)
                        </div> 
                            @php ($icol = 0)
                            @php ($irow++) 
                        @endif
                        @endforeach
                        @if($icol==12)
                        </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 participants-all">
                            <a href="/participants/{{$currentCategory->name}}">все участники</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-right">
                <div class="col-md-3 col-sm-12 col-xs-12 ">
                    <div class="video">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/NsXm6PC7aeM">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/46tv.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>  
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/Cmi1Ee-vIQs">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/takt.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>  
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/buZUi37FY6k">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/gtrkkursk.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>  
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/7wmejyrZQSo">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/samocveti.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>  
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/NM50C02Q34I">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/feoktistova_kristina.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/ShFOkHwXY80">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/regbi_kursk.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                 <a class="iframe" href="https://www.youtube.com/embed/CxsqQu9z1QA">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/malenkii_teatrik.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                    
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="iframe" href="https://www.youtube.com/embed/nHnJ-Ly6_f8">
                                    <div class="video-img">
                                        <img src="{{ asset('images/main_video/slavina_margarita.jpg') }}" />
                                        <div class="youtube-icon"></div>
                                    </div>
                                    
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- row-->  
    </div>   <!-- container-fluid-->
</div> <!--content main-->
@endsection
@section('footer')
    @parent
    <script src="{{ asset('js/fancybox/jquery.fancybox.min.js') }}"></script> 
    <script src="{{ asset('js/jmosaicflow/jquery.mosaicflow.min.js') }}"></script> 
    <script type="text/javascript" src="{{ asset('js/slick/slick.min.js') }}"></script>
    <script type="text/javascript"> 
        jQuery(document).ready(function() { 
          jQuery("a.fancyimage").fancybox({}); 
        });
        jQuery(document).ready(function() { 
          jQuery("a.iframe").fancybox({}); 
        })
        $(document).ready(function(){
          $('.gallery-slider').slick({
              autoplay: true,
              autoplaySpeed: 2000,
              accessibility: false,
              arrows: true,
              vertical: true,
              verticalSwiping: true,
              slidesToShow: 8
          });
        });
    </script>
    <script>
        jQuery(document).ready(function() { 
            jQuery('.actor-link').hover(function(){
                $(this).children('.actor-info').toggle();
            });
        });
    </script>
    
@endsection