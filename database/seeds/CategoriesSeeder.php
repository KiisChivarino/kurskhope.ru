<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
                      [
                        'name'=>'all',
                        'title'=>'Все категории',
                        'image'=>'all.jpg',
                        'article'=>'<p class="greeting-title">Дорогие друзья!<br/>Участники проекта<br/> «Будущее Соловьиного края. <br/>Курск 21 век»! </br>Родители, педагоги!</p>
                                  <p>Перед вами уникальный проект, который знакомит нас с теми, кто несмотря на свой молодой возраст, уже добились значительных успехов в разных сферах не только на региональном и всероссийском, но и на международном уровне.</p>
                                  <p>Стать первым, получить общественное признание всегда непросто. Но в Курской области очень много умных, талантливых, инициативных, энергичных и целеустремленных школьников, студентов и аспирантов, которые активно участвуют в творческой, научной, спортивной и политической жизни, достойно представляют Соловьиный край на мероприятиях различного уровня и своими победами вносят огромный вклад в укрепление имиджа России. Эти ребята уже сегодня определяют лицо нашего региона и страны, являются огромной движущей и созидательной силой.</p>
                                  <p>Целью нашего проекта мы ставили рассказать о каждом из них, привлечь внимание широкой общественности к их заслугам и достижениям, стимулировать саму молодежь к новым победам и свершениям.</p>
                                  <p>Убежден, что наш проект будет иметь продолжение, потому что земля Курская никогда не оскудеет талантами, и с каждым годом список лучших представителей молодежи будет пополняться все новыми и новыми именами.</p>
                                  <p>Желаю участникам проекта новых профессиональных побед, здоровья, жизненного оптимизма и уверенности в собственных силах. Курский край и Россия гордятся Вами! Особые слова благодарности – родителям, педагогам и наставникам ребят, которые вкладывают в них свои знания, время, силы и душу. Пусть настоящие и будущие успехи Ваших детей станут достойной наградой за Ваш труд, любовь, поддержку и терпение!</p>
                                  <p>Надеюсь, что знакомство посетителей с именами и биографиями лучших представителей курской молодежи будет содержательным, интересным и полезным.</p>
                                  <p class="greeting-writer">Депутат Государственной Думы Федерального Собрания Российской Федерации VII созыва Виктор Карамышев</p>',
                        'greeting_writer'=>'<p class="greeting-writer">Депутат Государственной Думы Федерального Собрания Российской Федерации VII созыва Виктор Карамышев</p>',
                        'video'=>'https://www.youtube.com/embed/SVVb8XV_7f8'
                       ],
                       [
                        'name'=>'science',
                        'title'=>'Наука',
                        'image'=>'science.jpg',
                        'article'=>'<p class="greeting-title">Юноши и девушки!</p>
          <p> Наша страна оказалась перед новыми вызовами времени. И для того чтобы достойно отвечать им, нужны молодые силы, энергия и знания. Сегодня для России время творцов, профессионалов и патриотов. Это ваше время, друзья! Молодежь — наше настоящее и будущее. Знания молодежи и ее стремление приносить пользу — важнейший ресурс для страны.</p>
          <p>Современный ритм жизни диктует необходимость тесной связи науки и практики, определяет новые тенденции в деятельности ученых страны. Наукоемкие технологии, активная молодежь и высококвалифицированные специалисты — вот будущее России. Сегодня любовь к Отечеству проявляется во вкладе каждого из нас в общее дело — построение гражданского общества, сохранение культурных и духовных ценностей, помощь людям, кропотливое создание научных разработок и открытий.</p>
          <p>В молодые годы наступает долгожданная свобода взрослой жизни, собственных решений, открываются новые горизонты, кипят энтузиазм и энергия, в голове сами собой рождаются планы, идеи, вокруг множество единомышленников. Увлечение наукой в юношеские годы у многих перерастает в серьезные исследования, сопровождается публикациями в печати, оформлением заявок на изобретения и патенты, заканчивается защитой кандидатских диссертаций.</p>
          <p>Мы гордимся тем, что современная молодежь стремится к научному поиску, составляет базу инновационного прогресса, смело принимает из рук наставников и учителей ответственность, эстафету созидания и творчества. Не бойтесь строить грандиозные планы, у вас есть всё для того, чтобы их выполнить!</p>
          <p> Мы уверены, вы с этим справитесь! Как и со всеми другими задачами, которые поставит перед вами жизнь и время!</p>
          <p>Желаю молодым людям страны, нашего региона яркой жизни, успехов, упорно идти к своей цели, не сворачивая с намеченного пути, заниматься любимым делом, чтобы страна была сильной и мощной.</p>
          <p> Мы искренне верим в ваш потенциал, в ваши силы и желание сделать мир лучше, хранить в душе огонь творчества, жажду знаний, поиска и открытий.</p>
          <p class="greeting-writer">Лазаренко Виктор Анатольевич,<br/> ректор ФГБОУ ВО «Курский государственный<br/> медицинский университет» Минздрава РФ,<br/> Заслуженный врач РФ, профессор, доктор медицинских наук</p>',
                        'greeting_writer'=>'<p class="greeting-writer"><p class="greeting-writer">Лазаренко Виктор Анатольевич,<br/> ректор ФГБОУ ВО «Курский государственный<br/> медицинский университет» Минздрава РФ,<br/> Заслуженный врач РФ, профессор, доктор медицинских наук</p>',
                        'video'=>'https://www.youtube.com/embed/5szNSUQSjeM'
                       ],
                                [
                        'name'=>'sport',
                        'title'=>'Спорт',
                        'image'=>'sport.jpg',
                        'article'=>'<p class="greeting-title">Дорогие друзья!</p>
          <p> Приветствую участников и организаторов проекта «Будущее Соловьиного края. Курск — 21 век», поставившего перед собой важную цель — рассказать широкой общественности о талантливой курской молодежи, активизировать участие молодого поколения в многообразных направлениях нашей жизни.</p>
          <p>Сегодня молодежь — это особая социальная группа с высоким интеллектуально-нравственным потенциалом, серьезными профессиональными и творческими возможностями, активно содействующая социально-экономическому, научному и культурному развитию страны. Ее энергия и инициатива способствуют глубоким преобразованиям, затрагивающим фактически все сферы жизни общества и государства, интересы каждого гражданина. И будущее нашей страны в немалой степени зависит именно от стремления молодежи к самореализации.</p>
          <p>А потому, обращаясь к участникам проекта «Будущее Соловьиного края. Курск — 21 век» и в их лице ко всем школьникам и студентам Курской области, хотелось бы сказать. Друзья! Ваше настоящее и будущее, место и роль в жизни общества определят целеустремленность и настойчивость, деловитость и профессионализм, доброта души и милосердие — все лучшие качества и свойства человеческой натуры. Надеюсь, что вы сумеете в полной мере овладеть ими и использовать их во имя благородных целей.</p>
          <p>Желаю вам больших творческих успехов, счастья, крепкого здоровья и новых побед!</p>
          <p class="greeting-writer">Чаплыгин Валерий Андреевич,<br/> чемпион XXI Олимпийских летних игр 1976 года <br/>в Монреале по велоспорту в командной гонке на 100 км<br/> (первый курянин — олимпийский чемпион),<br/> Заслуженный мастер спорта СССР,<br/> Заслуженный тренер СССР,<br/> Почётный гражданин города Курска</p>',
                       'greeting_writer'=>'<p class="greeting-writer">Чаплыгин Валерий Андреевич,<br/> чемпион XXI Олимпийских летних игр 1976 года <br/>в Монреале по велоспорту в командной гонке на 100 км<br/> (первый курянин — олимпийский чемпион),<br/> Заслуженный мастер спорта СССР,<br/> Заслуженный тренер СССР,<br/> Почётный гражданин города Курска</p>',
                       'video'=>'https://www.youtube.com/embed/q-F6m-rdsfw'
                       ],
                                [
                        'name'=>'sport_teams',
                        'title'=>'Спортивные команды',
                        'image'=>'sport_teams.jpg',
                        'article'=>'<p class="greeting-title">Уважаемые друзья!</p>
          <p>Рад приветствовать вас и поздравить с участием в проекте «Будущее Соловьиного края. Курск — 21 век», объединившем лучших представителей молодого поколения нашего региона!</p>
          <p>Приятно видеть фамилии ребят, посвятивших свою жизнь спорту и уже добившихся в нем серьезных результатов. Ведь спорт — это не только достижения, это прежде всего возможность показать свой позитивный жизненный настрой, активную позицию, готовность к серьезным испытаниям, выдержку, мастерство, собранность и стремление к победе. Это возможность заявить о желании добиваться поставленной цели, двигаться вперед. Спорт закаляет волю и характер, воспитывает лучшие человеческие качества, способствует утверждению в обществе ценностей здорового образа жизни.</p>
          <p>Юные спортсмены не только отстаивают честь Курской области на различных соревнованиях, но и способствуют дальнейшему развитию замечательных традиций отечественных школ в разных видах спорта, тем самым активно работают на становление здоровой нации в стране. Сегодня эти ребята — спортивная гордость школы, области, страны, а уже завтра, возможно, участники и победители Олимпийских игр.</p>
          <p>Желаю всем юным спортсменам честной и бескомпромиссной борьбы, спортивного азарта и достойных соперников! Благодарю тренеров и родителей ребят, с поддержкой которых им легче добиваться высоких результатов в спорте.</p>
          <p> Удачи, новых жизненных и профессиональных побед всем участникам проекта «Будущее Соловьиного края. Курск — 21 век»!</p>
          <p class="greeting-writer"> Мавлютов Ильдар Масалимович, <br/>Заслуженный работник физической культуры России,<br/> Заслуженный тренер России, почетный работник<br/> физической культуры и спорта Курской области, <br/>тренер-преподаватель Центра спортивной подготовки<br/> «Школа высшего спортивного мастерства»,<br/> тренер чемпионки мира Ольги Лобынцевой, <br/>олимпийских чемпионок Евгении Ламоновой <br/>и Инны Дериглазовой</p>',
                        'greeting_writer'=>'<p class="greeting-writer"> Мавлютов Ильдар Масалимович, <br/>Заслуженный работник физической культуры России,<br/> Заслуженный тренер России, почетный работник<br/> физической культуры и спорта Курской области, <br/>тренер-преподаватель Центра спортивной подготовки<br/> «Школа высшего спортивного мастерства»,<br/> тренер чемпионки мира Ольги Лобынцевой, <br/>олимпийских чемпионок Евгении Ламоновой <br/>и Инны Дериглазовой</p>',
                        'video'=>'https://www.youtube.com/embed/q-F6m-rdsfw'
                       ],
                                [
                        'name'=>'creative',
                        'title'=>'Творчество',
                        'image'=>'creative.jpg',
                        'article'=>'<p class="greeting-title">Уважаемые участники проекта «Будущее Соловьиного края. <br/> Курск – 21 век»! <br/>Дорогие земляки!</p>
          <p>Формула бесконечности — это наши дети. Талантливые дети России — это будущее страны. Я рад, что у нас в Курской области много талантливой молодежи, которая не только вкладывает свои время и силы в прославление родного края, но и продолжает славные традиции Курской земли, ставшей родиной выдающихся деятелей в области культуры и искусства — Г.В. Свиридова, Н.В. Плевицкой, В.В. Ивановского, М.Г. Эрденко, В.Г. Шварца, К.А. Трутовского и многих других. Мы гордимся вами!</p>
          <p>У России — героическое прошлое. Наши отцы и деды не жалели себя, защищая страну. И мы должны продолжить их наследие — опираясь на традиции и ценности, сложившиеся столетиями, сделать Россию сильной и духовно богатой!</p>
          <p> Уважаемые участники проекта «Будущее Соловьиного края. Курск — 21 век»!Желаю вам творческих успехов, удачи и не останавливаться на достигнутом!</p>
          <p class="greeting-writer">Проскурин Сергей Георгиевич,<br/> Заслуженный работник культуры РФ,<br/> действительный член (академик)<br/>Международной академии творчества, <br/>профессор Курского государственного университета,<br/> главный дирижер и художественный руководитель<br/> Русского камерного оркестра Курского<br/> государственного университета</p>',
                        'greeting_writer'=>'<p class="greeting-writer">Проскурин Сергей Георгиевич,<br/> Заслуженный работник культуры РФ,<br/> действительный член (академик)<br/>Международной академии творчества, <br/>профессор Курского государственного университета,<br/> главный дирижер и художественный руководитель<br/> Русского камерного оркестра Курского<br/> государственного университета</p>',
                        'video'=>'https://www.youtube.com/embed/CzEzYlPFcTs'
                       ],
                                [
                        'name'=>'creative_teams',
                        'title'=>'Творческие коллективы',
                        'image'=>'creative_teams.jpg',
                        'article'=>'<p class="greeting-title">Дорогие друзья! Рада приветствовать участников проекта «Будущее Соловьиного края. Курск — 21 век»!</p>
          <p>Курская область является самобытным культурным центром России, учреждения искусства и творческие коллективы которого пользуются заслуженной известностью далеко за пределами губернии. Необыкновенно богатое и разнообразное культурное пространство региона представляет собой уникальную среду, в которой рождаются и развиваются юные дарования. О них — эта книга! О талантливых ребятах, уже успевших достойно проявить себя на всероссийском и международном уровнях.</p>
          <p>«Будущее Соловьиного края. Курск — 21 век» — уникальный проект. Он рассказывает нам о самых разных людях — музыкантах, танцорах, спортсменах, молодых ученых, но всех их объединяет одно — несмотря на свой юный возраст, они уже состоялись как личности! Они обладают очень важными качествами — целеустремленностью, необыкновенной работоспособностью, огромным творческим потенциалом.</p>
          <p> Сердечно благодарю организаторов проекта, которые делают большое благородное дело! Спасибо вам за ваш энтузиазм и профессионализм!</p>
          <p> От всей души желаю всем участникам проекта вдохновения и творческих открытий, новых побед и удачи во всем!</p>
          <p class="greeting-writer">Крыгина Надежда Евгеньевна,<br/> Народная артистка России</p>',
                        'greeting_writer'=>'<p class="greeting-writer">Крыгина Надежда Евгеньевна,<br/> Народная артистка России</p>',
                        'video'=>'https://www.youtube.com/embed/CzEzYlPFcTs'
                       ],
                                [
                        'name'=>'kursk_hopes',
                        'title'=>'Надежды курского края',
                        'image'=>'kursk_hopes.jpg',
                        'article'=>'<p class="greeting-title">Уважаемые читатели!</p>
          <p> Вы держите в руках энциклопедию «Будущее Соловьиного края», новый уникальный проект, в котором собраны достижения молодого поколения Курской области.</p>
          <p>Древний Курский край всегда славился замечательными людьми — учеными, политиками, творческими деятелями, спортсменами.</p>
          <p>Собирая материал для этой книги, мы еще раз убедились в том, насколько богата земля Курская талантами и что молодое поколение курян с честью принимает эстафету от старших по прославлению своей малой родины.</p>
          <p> По роду моей предпринимательской и общественной деятельности мне выпала возможность поработать на родине моих предков и вложить свою толику в процветание славной Курской земли.</p>
          <p>Перед вами раздел «Надежды Курского края», в котором собраны рассказы о ребятах, не имеющих длинного списка наград и регалий — они пока в начале пути, но уже подают большие надежды. Уверен, каждый из них впишет достойную страницу в историю Соловьиного края и даст повод своим землякам гордиться его достижениями и добрыми делами. Вместе нам всё по плечу!</p>
          <p class="greeting-writer">Малахов Олег Игоревич, <br/>член Генерального совета Общероссийской <br/>общественной организации «Деловая Россия», <br/>генеральный директор ООО «УНИКА инжиниринг»</p>',
                        'greeting_writer'=>'<p class="greeting-writer">Малахов Олег Игоревич, <br/>член Генерального совета Общероссийской <br/>общественной организации «Деловая Россия», <br/>генеральный директор ООО «УНИКА инжиниринг»</p>',
                        'video'=>'https://www.youtube.com/embed/SVVb8XV_7f8'
                       ]
    ]);
    }
}
