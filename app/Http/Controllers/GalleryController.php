<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function show($type=null){
        $categories = DB::select("SELECT id, name, title, image, article from categories");
        $files = scandir($_SERVER['DOCUMENT_ROOT'].'/public/images/gallery/fullSize/');
        foreach($files as $key=>$value){
            if(strpos($value,'.jpg')===false) unset($files[$key]);
        }
        //dump($files);
        $array = array(
                    'categories'=>$categories,
                    'images'=>$files
                );
                return view('gallery',$array);
    }
}
