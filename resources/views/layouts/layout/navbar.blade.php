            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand и toggle сгруппированы для лучшего отображения на мобильных дисплеях -->  
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>

                <!-- Соберите навигационные ссылки, формы, и другой контент для переключения -->  
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Главная</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Участники<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        @foreach($categories as $category)
                        <li><a href="/participants/{{$category->name}}">{{$category->title}}</a></li>
                        @endforeach
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Международные<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        @foreach($categories as $category)
                        <li><a href="/participants/{{$category->name}}/summit">{{$category->title}}</a></li>
                        @endforeach
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Российские<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        @foreach($categories as $category)
                        @if(!($category->name == 'sport_teams' or $category->name == 'creative_teams'))<li><a href="/participants/{{$category->name}}/russian">{{$category->title}}</a></li>@endif
                        @endforeach
                      </ul>
                    </li>
                    <li><a href="http://vkaramyshev.ru/kontakty/obshchestvennaya-priemnaya" target="_blank">Организаторы</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Галерея<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="{{url('/gallery/photo')}}">Фото</a></li>
                        <li><a href="{{url('/gallery/video')}}">Видео</a></li>
                      </ul>
                    </li>
                    <li><a href="{{url('/contacts')}}">Контакты</a></li>
                  </ul>
                  <form class="navbar-form navbar-right" action="{{url('/searchredirect')}}">
                    <div class="form-group">
                      <input type="text" class="form-control" name="search" placeholder="Поиск" />
                    </div>
                    <button type="submit" class="btn btn-default">Найти</button>
                  </form>
                </div><!-- /.navbar-collapse -->  
              </div><!-- /.container-fluid -->  
            </nav>