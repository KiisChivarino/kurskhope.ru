<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ParticipantController extends Controller
{
    public function show($category='all', $pid){
        $categories = DB::select("SELECT id, name, title from categories");
        $categoryFilter = '';
        
        $actor = DB::select("
            						SELECT
                                           a.name as actor_name, 
            							   a.image,
            							   a.article,
            							   p.birth_date, 
            							   c.title as category_name, 
            							   l.title as level_name 
            							   FROM participants AS p
            						LEFT JOIN actors AS a ON p.id=a.id
            						LEFT JOIN categories AS c ON p.categories_id=c.id
                                    LEFT JOIN levels AS l ON p.levels_id=l.id
                                    WHERE p.id = $pid
            					");
        
        $currentCategoryTitle = 'Все участники';
        foreach($categories as $category_item){
        	if($category_item->name == $category){
        		$currentCategoryTitle = $category_item->title;
        		break;
        	}
        }
        
        $array = array(
                'categories'=>$categories,
                'currentCategory'=>$category,
                'currentCategoryTitle'=>$currentCategoryTitle,
                'actor'=>$actor[0],
                //'search'=>function(){return 'search';}
            );
        	//dump($array['actor']);
            return view('participant',$array);
			
    }
}
