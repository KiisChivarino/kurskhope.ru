<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'IndexController@show');
Route::get('/searchredirect',function(){
    if(empty(Input::get('search'))) return redirect()->back();
    $search = urlencode(Input::get('search'));
    $route = "/search/$search";
    return redirect($route);
});
Route::get('/search/{search}','SearchController@search');
Route::get('/contacts','ContactsController@show');
Route::post('/contacts','ContactsController@form');
Route::get('/gallery/{type}','GalleryController@show');
Route::get('/participants/{category}/{id}', 'ParticipantController@show')->where('id','[0-9]+');
Route::get('/participants','ParticipantsController@show');
Route::get('/participants/{category}/{level?}', 'ParticipantsController@show')->where('level','[a-z]+');

Route::get('/{category?}', 'IndexController@show');

//Route::get('/participants', 'ParticipantsController@show');



