<?php

use Illuminate\Database\Seeder;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert([
        									['name'=>'russian','title'=>'Российские'],
        									['name'=>'summit','title'=>'Международные'],
        								  ]);
    }
}
