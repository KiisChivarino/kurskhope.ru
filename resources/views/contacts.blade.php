@extends('layouts.layout')
@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/contacts.css') }}" />
@endsection
@section('header')
	@parent
@endsection
@section('content')
        <div class="content contacts">
            <div class="container">
                <div class="row">
                    <div class="coll-md-12 coll-sm-12 coll-xs-12 title">
                        <h1>Контакты</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="coll-md-12 coll-sm-12 coll-xs-12 info">
                        <b>Адрес:</b>
                        г. Курск, ул. Радищева, 17, каб. 304<br/>
                         <b>Телефон: </b> 8 (4712) 70-34-32<br/>
                         <b>E-mail: </b> kursk.karamyshev@yandex.ru
                    </div>
                </div>
                <div class="row">
                    <div class="coll-md-12 coll-sm-12 coll-xs-12 map">
                        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A3d4d34180cef768def147d0898912d01d72a8a84bf8ee4642df78311ce453b13&amp;width=100%25&amp;height=322&amp;lang=ru_RU&amp;scroll=true"></script>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('footer')
	@parent
@endsection