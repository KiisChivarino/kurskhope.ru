<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $whereTag = ' WHERE ';

    public function show($category="all"){
        $categories = DB::select("SELECT id, name, title, image, article, greeting_writer, video from categories");
        
        $currentCategory = null;
        foreach($categories as $cat_item){
            if($cat_item->name == $category) {$currentCategory = $cat_item;
            break;}
        }

        $categoryFilter = '';
        
        if($category!='all') {
            $categoryFilter = $this->whereTag.'c.name='.'"'.$category.'"';
            $this->whereTag = ' AND ';
        }
        $typeFilter = $this->whereTag.'a.types_id != 1 ';
        $this->whereTag = ' AND ';
        $levelFilter = $this->whereTag.'l.id = 2 ';
        if($category == 'sport_teams' or $category == 'creative_teams') $typeFilter = '';
        if($category == 'kursk_hopes') $levelFilter = '';
        $actors = DB::select("
                                        SELECT 
                                               p.id, 
                                               a.mini_image, 
                                               a.name as actor_name, 
                                               c.name as category_name,
                                               c.title as category_title, 
                                               p.birth_date, 
                                               l.title as level_name
                                        FROM `participants` AS p
                                        LEFT JOIN `actors` AS a ON p.id=a.id
                                        LEFT JOIN categories AS c ON p.categories_id=c.id
                                        LEFT JOIN levels AS l ON p.levels_id=l.id
                                        $categoryFilter
                                        $typeFilter
                                        
                                    ");
        $main_photo = $this->getImages();
        $array = array(
                    'actors'=>$actors,
                    'categories'=>$categories,
                    'currentCategory'=>$currentCategory,
                    'main_photo'=>$main_photo
                );
                return view('index',$array);
    }
    
    public function getImages(){
        $files = scandir($_SERVER['DOCUMENT_ROOT'].'/public/images/main_photo/fullSize/');
        foreach($files as $key=>$value){
            if(strpos($value,'.jpg')===false) unset($files[$key]);
        }
        return $files;
    }
}
