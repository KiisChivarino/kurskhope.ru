<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Будущее соловьиного края</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}" />
        <style>
           
        </style>
    </head>
    <body>
        @section('header')
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-3 col-xs-12 logo-img"><img src="{{ asset('images/logo.png') }}"/></div>
                    <div class="col-md-8 col-sm-9 col-xs-12 title"><h1>Будущее соловьиного края</h1></div>
                    <div class="col-md-2 hidden-sm hidden-xs birds"><img src="{{ asset('images/birds.png') }}"/></div>
                </div>
            </div>
            @section('navMenu') 
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand и toggle сгруппированы для лучшего отображения на мобильных дисплеях -->  
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>

                <!-- Соберите навигационные ссылки, формы, и другой контент для переключения -->  
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Главная</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Участники<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Все</a></li>
                        <li><a href="#">Наука</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Спорт</a></li>
                        <li><a href="#">Спортивные команды</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Творчество</a></li>
                        <li><a href="#">Творческие команды</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Надежды курского края</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Организаторы</a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Галерея<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">Фото</a></li>
                        <li><a href="#">Видео</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Контакты</a></li>
                  </ul>
                  <form class="navbar-form navbar-right">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Поиск">
                    </div>
                    <button type="submit" class="btn btn-default">Найти</button>
                  </form>
                </div><!-- /.navbar-collapse -->  
              </div><!-- /.container-fluid -->  
            </nav>
            @show
        </div>
        @show
        @section('content')
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12 participants">
                        <div class="row">
                            <div class="col-md-2 col-sm-12 col-xs-12 participants-categories">
                                <ul class="list-group">
                                  <li class="list-group-item">Все</li>
                                  <li class="list-group-item">Наука</li>
                                  <li class="list-group-item">Спорт</li>
                                  <li class="list-group-item">Спортивные команды</li>
                                  <li class="list-group-item">Творчество</li>
                                  <li class="list-group-item">Творческие команды</li>
                                  <li class="list-group-item">Надежды курского края</li>
                                </ul>
                            </div>
                            <div class="col-md-10 col-sm-12 col-xs-12 participants-content">
                                <div class="greeting">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="greeting-text">
                                                <p class="greeting-title">Дорогие друзья!<br/>Участники проекта<br/> «Будущее Соловьиного края. <br/>Курск - 21 век»! </br>Родители, педагоги!</p>
                                                <p>Перед вами уникальный проект, который знакомит нас с теми, кто несмотря на свой молодой возраст, уже добились значительных успехов в разных сферах не только на региональном и всероссийском, но и на международном уровне.</p>
                                                <p>Стать первым, получить общественное признание всегда непросто. Но в Курской области очень много умных, талантливых, инициативных, энергичных и целеустремленных школьников, студентов и аспирантов, которые активно участвуют в творческой, научной, спортивной и политической жизни, достойно представляют Соловьиный край на мероприятиях различного уровня и своими победами вносят огромный вклад в укрепление имиджа России. Эти ребята уже сегодня определяют лицо нашего региона и страны, являются огромной движущей и созидательной силой.</p>
                                                <p>Целью нашего проекта мы ставили рассказать о каждом из них, привлечь внимание широкой общественности к их заслугам и достижениям, стимулировать саму молодежь к новым победам и свершениям.</p>
                                                <p>Убежден, что наш проект будет иметь продолжение, потому что земля Курская никогда не оскудеет талантами, и с каждым годом список лучших представителей молодежи будет пополняться все новыми и новыми именами.</p>
                                                <p>Желаю участникам проекта новых профессиональных побед, здоровья, жизненного оптимизма и уверенности в собственных силах. Курский край и Россия гордятся Вами! Особые слова благодарности – родителям, педагогам и наставникам ребят, которые вкладывают в них свои знания, время, силы и душу. Пусть настоящие и будущие успехи Ваших детей станут достойной наградой за Ваш труд, любовь, поддержку и терпение!</p>
                                                <p>Надеюсь, что знакомство посетителей с именами и биографиями лучших представителей курской молодежи будет содержательным, интересным и полезным.</p>
                                                <p class="greeting-writer">Депутат Государственной Думы Федерального Собрания Российской Федерации VII созыва Виктор Карамышев</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="greeting-photo"><img src="{{ asset('images/organizers/karamishev.jpg') }}"/></div>
                                        </div>
                                    </div>
                                    <div class="participants-video">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <iframe width="100%" height="400px" src="https://www.youtube.com/embed/SVVb8XV_7f8" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="participants-children">
                                        <div class="row">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/1.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/2.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/3.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/4.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/5.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/6.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/7.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/8.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/9.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/10.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/11.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/12.jpg') }}"/></a></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/13.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/14.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/15.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/16.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/17.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/18.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/19.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/20.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/21.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/22.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/23.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/24.jpg') }}"/></a></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/25.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/26.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/27.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/28.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/29.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/30.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/31.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/32.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/33.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/34.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/35.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/36.jpg') }}"/></a></div>
                                        </div>
                                        <div class="row hidden-xs">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/37.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/38.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/39.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/40.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/41.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/42.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/43.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/44.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/45.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/46.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/47.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/48.jpg') }}"/></a></div>
                                        </div>
                                        <div class="row hidden-xs">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/49.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/50.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/51.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/52.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/53.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/54.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/55.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/56.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/57.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/58.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/59.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/60.jpg') }}"/></a></div>
                                        </div>
                                        <div class="row hidden-xs">
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/61.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/62.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/63.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/64.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/65.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/66.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/67.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/68.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/69.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/70.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/71.jpg') }}"/></a></div>
                                            <div class="col-md-1 col-sm-1 col-xs-2"><a href="#"><img src="{{ asset('images/persons/mini/72.jpg') }}"/></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 organizers">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 organizer-title">Организаторы проекта</div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                               <a href="#"> <img src="{{ asset('images/organizers/1.png') }}"/></a>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                               <a href="#"> <img src="{{ asset('images/organizers/2.png') }}"/></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                                <a href="#"> <img src="{{ asset('images/organizers/3.png') }}"/></a>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                               <a href="#">  <img src="{{ asset('images/organizers/4.png') }}"/></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                                <a href="#"><img src="{{ asset('images/organizers/5.png') }}"/></a>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                                <a href="#"><img src="{{ asset('images/organizers/6.png') }}"/></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-6 col-xs-12 organizer">
                                <a href="#"><img src="{{ asset('images/organizers/7.png') }}"/></a>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-9 col-sm-12 col-xs-12 participants-all"><a href="#">все участники</a></div>
                    <div class="col-md-3 col-sm-12 col-xs-12 organizers-all"><a href="#">все организаторы</a></div>
                </div>
            </div>
        </div>
        @show
        
        @section('footer')
        <div class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12 book"><a href="{{ asset('files/kursk_hope_book.pdf') }}" target="_blank">Скачать книгу</a></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 copyright"><span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"></span> Все права защищены</div>
                            <div class="col-md-3 col-sm-3 col-xs-12 contacts">
                                <a href="#"><span class="contacts-icon vk"></span></a>
                                <a href="#"><span class="contacts-icon youtube"></span></a>
                                <a href="#"><span class="contacts-icon email"></span></a>
                            </div>
                        </div>
                    </div>
        </div>
        @show
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('js/bootstrap.js') }}"></script>
    </body>
</html>